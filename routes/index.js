var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
/* GET Hello World page. */
router.get('/helloworld', function(req, res) {
  res.render('helloworld', { title: 'Hello, World!' });
});
/*Table1*/
router.get('/table', function(req, res) {
  var db = req.db;
  var collection = db.get('studentdetails');
  collection.find({},function(e,docs){
    // console.log(docs)
      res.render('table', {
          "table" : docs
      });
  });
});

  
/*Update*/
router.get('/update/:id', (req, res) => {    
  var ObjectID = require('mongodb').ObjectID;
  var id = req.params.id;
  var o_id = new ObjectID(id);
  var db = req.db;
  var collection = db.get('studentdetails');
   //var o_id = new ObjectId(id);
  collection.findOne({_id:o_id},(err, result)=> {
     if (err) return console.log(err)
     console.log("find to update",result);
     res.render('update.ejs',{studentdetails: result});  
  });
  console.log(req.params._id);
  });
  
  router.post('/updated/:id',(req, res) => {
    console.log("updated ID",req.params.id);
    var ObjectID = require('mongodb').ObjectID;
    var db = req.db;
    var id = req.params.id;
  var o_id = new ObjectID(id);
  var collection = db.get('studentdetails');
   collection.findOneAndUpdate({_id : o_id}, {$set: {
      "studentName": req.body.studentName,
      "className": req.body.className,
      "age": req.body.age,
      "marks" : req.body.marks,
      "totalMarks":req.body.totalMarks
      
   }
  
   });
       res.redirect('/table');
  });
/*create*/
router.get('/create', function(req, res) {
  res.render('create', { title: 'create form' });
});

module.exports = router;
/* GET Userlist page. */
router.get('/userlist', function(req, res) {
  var db = req.db;
  var collection = db.get('usercollection');
  collection.find({},{},function(e,docs){
      res.render('userlist', {
          "userlist" : docs
      });
  });
});

/* GET New User page. */
router.get('/newuser', function(req, res) {
  res.render('newuser', { title: 'Add New User' });
});
/* POST to Add User Service */
router.post('/adduser', function(req, res) {

    // Set our internal DB variable
    var db = req.db;

    // Get our form values. These rely on the "name" attributes
    var userName = req.body.username;
    var userEmail = req.body.useremail;

    // Set our collection
    var collection = db.get('usercollection');

    // Submit to the DB
    collection.insert({
        "username" : userName,
        "email" : userEmail
    }, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            // And forward to success page
            res.redirect("userlist");
        }
    });

});

router.post('/create', function(req, res) {

  // Set our internal DB variable
  var db = req.db;

  // Get our form values. These rely on the "name" attributes
  var studentName = req.body.studentName;
  var className = req.body.className;
  var age = req.body.age;
  var marks = req.body.marks;
  var  totalMarks = req.body.totalMarks;


  // Set our collection
  var collection = db.get('studentdetails');

  // Submit to the DB
  collection.insert({
      "studentName" : studentName,
      "className" : className,
      "age"       : age, 
      "marks"      : marks,
      "totalMarks":totalMarks
  }, function (err, doc) {
      if (err) {
          // If it failed, return error
          res.send("There was a problem adding the information to the database.");
      }
      else {
          // And forward to success page
          res.redirect("table");
      }
  });

});

//Delete
router.get('/delete/:id', function (req,res){

var ObjectID = require('mongodb').ObjectID;
  var id = req.params.id;
    var o_id = new ObjectID(id);
     var db = req.db;
      var collection = db.get('studentdetails');

          collection.remove(
              { _id:o_id}
                // console.log(docs)
                  
          );    
            res.redirect("/table");
});